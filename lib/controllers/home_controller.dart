import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';

import '../model/home_model.dart';
import '../services/api_service.dart';

class AppConfigurationController extends GetxController {
  void onInit() {
    super.onInit();
    getData();
  }

  Rx<DateTime> selectedDate = DateTime.now().obs;
  getData() async {
    try {
      isLoading.value = true;
      data.addAll(await ApiService.getData());
      // data.addAll(await ApiServices.getData());
      // data.addAll(await ApiServices.getData());

      isError.value = false;
    } catch (error) {
      isError.value = true;
    } finally {
      isLoading.value = false;
    }
  }

  List<ApiResponseModel> data = <ApiResponseModel>[];
  static AppConfigurationController get to => Get.find();
  RxBool isLoading = false.obs;
  RxBool isError = false.obs;
}
