import 'package:datagridproject/model/home_model.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';

class InnerPageController extends GetxController {
  ApiResponseModel data = Get.arguments;
  static InnerPageController get to => Get.find();
}
