import 'package:datagridproject/model/home_model.dart';
import 'package:http/http.dart' as http;

class ApiService {
  static Future<List<ApiResponseModel>> getData() async {
    http.Response response = await http.post(
      Uri.parse("https://run.mocky.io/v3/d49cdbcd-e611-4855-912c-9fbaa09df7b2"),
    );
    if (response.statusCode == 200) {
      return apiResponseModelFromJson(response.body);
    } else {
      throw Exception(response.body);
    }
  }
}
