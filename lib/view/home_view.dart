import 'package:datagridproject/controllers/home_controller.dart';
import 'package:datagridproject/view/inner_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class TableContainer extends StatelessWidget {
  const TableContainer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(AppConfigurationController());
    return SafeArea(
      child: Scaffold(
        body: GetX<AppConfigurationController>(builder: (controller) {
          return controller.isLoading.value
              ? const CircularProgressIndicator()
              : controller.isError.value
                  ? Text("error")
                  : SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: DataTable(
                          border: TableBorder.all(color: Colors.black12),
                          headingRowColor: MaterialStateColor.resolveWith(
                            (states) {
                              return Color.fromRGBO(1, 59, 108, 1);
                            },
                          ),
                          dataRowMaxHeight: 130,
                          horizontalMargin: 0,
                          showBottomBorder: true,
                          columnSpacing: 0,
                          rows: AppConfigurationController.to.data
                              .map((e) => DataRow(
                                    color: MaterialStateColor.resolveWith(
                                      (states) {
                                        return (e.active ?? false)
                                            ? Colors.transparent
                                            : Colors.grey;
                                      },
                                    ),
                                    cells: [
                                      DataCell(GestureDetector(
                                        onTap: () {
                                          if (e.active ?? false) {
                                            Get.to(InnerPageView(),
                                                arguments: e);
                                          }
                                        },
                                        child: Container(
                                            width: 40,
                                            child:
                                                Text((e.id ?? 0).toString())),
                                      )),
                                      DataCell(Text((e.itemId ?? ""))),
                                      DataCell(Container(
                                          margin: EdgeInsets.all(5),
                                          width: 200,
                                          child: Text((e.title ?? "")))),
                                      DataCell(Row(
                                        children: [
                                          Container(
                                            child: GestureDetector(
                                              onTap: () async {
                                                if (e.overdue ?? false) {
                                                } else {
                                                  DateTime? date =
                                                      await showDatePicker(
                                                          context: context,
                                                          firstDate:
                                                              DateTime(1998),
                                                          lastDate:
                                                              DateTime(2025),
                                                          initialDate:
                                                              DateTime.now());
                                                  // chooseDate();
                                                  e.date?.value =
                                                      date ?? DateTime.now();
                                                }
                                              },
                                              child: Icon(
                                                Icons.calendar_month,
                                                color: (e.overdue ?? false)
                                                    ? Colors.green
                                                    : Colors.red,
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            width: 20,
                                          ),
                                          Container(
                                            width: 140,
                                            child: Obx(() => dateFormat(
                                                e.date?.value ??
                                                    DateTime.now())),
                                          ),
                                        ],
                                      )),
                                      DataCell(Padding(
                                        padding:
                                            const EdgeInsets.only(left: 10),
                                        child: Container(
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(4),
                                            color: Colors.red,
                                          ),
                                          height: 20,
                                          width: (e.status?.currentCount ?? 0)
                                              .toDouble(),
                                        ),
                                      )),
                                      DataCell(Container(
                                        width: 200,
                                        child: Row(
                                          children: [
                                            Container(
                                                width: 100,
                                                decoration: const BoxDecoration(
                                                    border: Border(
                                                        right: BorderSide(
                                                            color: Colors
                                                                .black12))),
                                                alignment: Alignment.center,
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            4),
                                                    color:
                                                        (e.itemType1?.color ??
                                                                "#00000000")
                                                            .toColor(),
                                                  ),
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            2.0),
                                                    child: Text(
                                                        e.itemType1?.value ??
                                                            ""),
                                                  ),
                                                )),
                                            Container(
                                                width: 100,
                                                alignment: Alignment.center,
                                                child: Container(
                                                  decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            4),
                                                    color:
                                                        (e.itemType2?.color ??
                                                                "#00000000")
                                                            .toColor(),
                                                  ),
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            2.0),
                                                    child: Text(
                                                        e.itemType2?.value ??
                                                            ""),
                                                  ),
                                                ))
                                          ],
                                        ),
                                      )),
                                      DataCell(Row(
                                        children: [
                                          Container(
                                            decoration: const BoxDecoration(
                                                border: Border(
                                                    right: BorderSide(
                                                        color:
                                                            Colors.black12))),
                                            width: 150,
                                            alignment: Alignment.center,
                                            child: Wrap(
                                              children: (e.level1 ?? [])
                                                  .map((e) => Container(
                                                      margin: EdgeInsets.all(3),
                                                      decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(4),
                                                        color: (e.color ??
                                                                "#ffffff")
                                                            .toColor(),
                                                      ),
                                                      child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .all(3.0),
                                                        child:
                                                            Text(e.value ?? ""),
                                                      )))
                                                  .toList(),
                                            ),
                                          ),
                                          Container(
                                            width: 150,
                                            alignment: Alignment.center,
                                            child: Wrap(
                                              children: (e.level2 ?? [])
                                                  .map((e) => Container(
                                                      margin: EdgeInsets.all(5),
                                                      decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(4),
                                                        color: (e.color ??
                                                                "#de1c1c")
                                                            .toColor(),
                                                      ),
                                                      child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .all(3.0),
                                                        child:
                                                            Text(e.value ?? ""),
                                                      )))
                                                  .toList(),
                                            ),
                                          ),
                                        ],
                                      ))
                                    ],
                                  ))
                              .toList(),
                          columns: [
                            const DataColumn(
                                label: Padding(
                              padding: EdgeInsets.only(left: 3),
                              child: Text(
                                "ID",
                                style: TextStyle(color: Colors.white),
                              ),
                            )),
                            const DataColumn(
                                label: Padding(
                              padding: EdgeInsets.only(left: 3),
                              child: Text(
                                "ITEM ID",
                                style: TextStyle(color: Colors.white),
                              ),
                            )),
                            const DataColumn(
                                label: Padding(
                              padding: EdgeInsets.only(left: 4),
                              child: Text(
                                "TITLE",
                                style: TextStyle(color: Colors.white),
                              ),
                            )),
                            const DataColumn(
                                label: Padding(
                              padding: EdgeInsets.only(left: 3),
                              child: Text(
                                "Date",
                                style: TextStyle(color: Colors.white),
                              ),
                            )),
                            const DataColumn(
                                label: Padding(
                              padding: EdgeInsets.only(left: 10, right: 100),
                              child: Text(
                                "Status",
                                style: TextStyle(color: Colors.white),
                              ),
                            )),
                            DataColumn(
                                label: Column(
                              children: [
                                SizedBox(
                                  height: 5,
                                ),
                                Container(
                                  width: 200,
                                  decoration: BoxDecoration(
                                    border: Border(
                                        bottom:
                                            BorderSide(color: Colors.black12)),
                                  ),
                                  child: Center(
                                    child: const Text(
                                      "ITEM type",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                const Row(
                                  children: [
                                    SizedBox(
                                        width: 100,
                                        child: Center(
                                            child: Text(
                                          "item 1",
                                          style: TextStyle(color: Colors.white),
                                        ))),
                                    SizedBox(
                                        width: 100,
                                        child: Center(
                                            child: Text(
                                          "item 2",
                                          style: TextStyle(color: Colors.white),
                                        ))),
                                  ],
                                )
                              ],
                            )),
                            DataColumn(
                                label: Column(
                              children: [
                                const SizedBox(
                                  height: 5,
                                ),
                                Container(
                                  width: 300,
                                  decoration: const BoxDecoration(
                                    border: Border(
                                        bottom:
                                            BorderSide(color: Colors.black12)),
                                  ),
                                  child: const Center(
                                    child: Text(
                                      "Level",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  children: [
                                    Container(
                                        width: 150,
                                        child: Center(
                                            child: Text(
                                          "Level 1",
                                          style: TextStyle(color: Colors.white),
                                        ))),
                                    Container(
                                        width: 150,
                                        child: Center(
                                            child: Text(
                                          "Level 1",
                                          style: TextStyle(color: Colors.white),
                                        ))),
                                  ],
                                )
                              ],
                            )),
                          ],
                        ),
                      ),
                    );
        }),
      ),
    );
  }
}

extension ColorExtension on String {
  toColor() {
    var hexString = this;
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }
}

dateFormat(DateTime date) {
  var suffix = "th";
  var digit = date.day % 10;
  if ((digit > 0 && digit < 4) && (date.day < 11 || date.day > 13)) {
    suffix = ["st", "nd", "rd"][digit - 1];
  }
  return RichText(
    text: TextSpan(children: [
      TextSpan(
          text: DateFormat("d").format(date),
          style: TextStyle(color: Colors.black)),
      WidgetSpan(
        child: Transform.translate(
          offset: Offset(0, -7),
          child: Text(
            suffix,
            style: TextStyle(fontSize: 10),
          ),
        ),
      ),
      TextSpan(
          text: DateFormat(" MMMM yyyy").format(date),
          style: TextStyle(color: Colors.black)),
    ]),
  );
  // return new DateFormat(" d'$suffix' MMMM yyyy").format(date);
}
