import 'package:datagridproject/utils/images.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../controllers/inner_page_controller.dart';

class InnerPageView extends StatelessWidget {
  const InnerPageView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(InnerPageController());
    return Scaffold(
      body: innerPageBody(),
    );
  }
}

Widget innerPageBody() {
  return Column(
    children: [
      Container(
        height: 250,
        width: Get.width,
        padding: EdgeInsets.only(left: 30, right: 30),
        decoration: BoxDecoration(
            gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          stops: [
            0.1,
            0.4,
            0.6,
            0.9,
          ],
          colors: [
            Color.fromRGBO(63, 166, 69, 1),
            Color.fromRGBO(50, 126, 131, 1),
            Color.fromRGBO(30, 72, 73, 1),
            Color.fromRGBO(15, 27, 28, 1),
          ],
        )),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 90,
            ),
            Text(
              InnerPageController.to.data.itemId ?? "",
              style: TextStyle(color: Colors.white),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              InnerPageController.to.data.title ?? "",
              style: TextStyle(color: Colors.white),
            ),
          ],
        ),
      ),
      SizedBox(
        height: 20,
      ),
      Container(
        width: Get.width,
        height: 250,
        margin: EdgeInsets.only(left: 20, right: 20),
        padding: EdgeInsets.only(left: 20, right: 20),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            border: Border.all(color: Colors.black12, width: 2)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 15,
            ),
            Text(
              "Status",
              style: TextStyle(
                  color: Color.fromRGBO(1, 59, 108, 1),
                  fontSize: 20,
                  fontWeight: FontWeight.w600),
            ),
            SizedBox(
              height: 10,
            ),
            Text("Current Count"),
            SizedBox(
              height: 5,
            ),
            Row(
              children: [
                Container(
                  height: 25,
                  width: InnerPageController.to.data.status?.currentCount
                          ?.toDouble() ??
                      0,
                  decoration: BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(4)),
                ),
                SizedBox(
                  width: 5,
                ),
                Text(
                  InnerPageController.to.data.status?.currentCount
                          ?.toString() ??
                      "",
                  style: TextStyle(color: Colors.black54),
                )
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Text("Total Count"),
            SizedBox(
              height: 5,
            ),
            Row(
              children: [
                Container(
                  height: 25,
                  width: InnerPageController.to.data.status?.currentCount
                          ?.toDouble() ??
                      0,
                  decoration: BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(4)),
                ),
                SizedBox(
                  width: 5,
                ),
                Text(
                  InnerPageController.to.data.status?.currentCount
                          ?.toString() ??
                      "",
                  style: TextStyle(color: Colors.black54),
                )
              ],
            ),
          ],
        ),
      ),
      SizedBox(
        height: 20,
      ),
      Container(
        width: Get.width,
        height: 250,
        margin: EdgeInsets.only(left: 20, right: 20),
        padding: EdgeInsets.only(left: 20, right: 20),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            border: Border.all(color: Colors.black12, width: 2)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 20,
            ),
            Text(
              "Lorem Ipsum",
              style: TextStyle(
                  color: Color.fromRGBO(1, 59, 108, 1),
                  fontSize: 20,
                  fontWeight: FontWeight.w600),
            ),
            SizedBox(
              height: 20,
            ),
            Image.asset(donutChartImage),
          ],
        ),
      ),
    ],
  );
}
